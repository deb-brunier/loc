import {
    Chart,
    ArcElement,
    BarElement,
    BarController,
    PieController,
    CategoryScale,
    LinearScale,
    Legend,
    Title,
    Tooltip,
} from 'chart.js';
Chart.register(
    ArcElement,
    BarElement,
    BarController,
    PieController,
    CategoryScale,
    LinearScale,
    Legend,
    Title,
    Tooltip,
);

  const chart1 = document.querySelector('#chart1')
  const days = chart1.dataset.days.split(',')
  const incomes = chart1.dataset.incomes.split(',').map(x => +x)

  new Chart(document.querySelector('#chart1'), {
    type: 'bar',
    data: {
        labels: days,
        datasets: [{
            label: 'Revenu par jour',
            data: incomes,
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: 'rgb(54, 162, 235)',
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    },
});

const chart2 = document.querySelector('#chart2')
const reserved = chart2.dataset.reserved
const total = chart2.dataset.total
const data = [reserved, total - reserved]
new Chart(document.querySelector('#chart2'), {
    type: 'pie',
    data: {
            labels: [
                'Occupé',
                'Libre'
            ],
            datasets: [{
            label: 'Taux d\'occupation',
            data: data,
            backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)'
            ],
        }]
    }
})