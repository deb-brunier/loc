<?php

namespace App\Security\Voter;

use App\Entity\Reservation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ReservationVoter extends Voter
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === 'RESERVATION_CANCEL' && $subject instanceof Reservation;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ($subject->getStatus() === -1) {
            return false;
        }

        if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
            return true;
        }

        switch ($attribute) {
            case 'RESERVATION_CANCEL':
                return $this->canCancel($subject);
        }

        return false;
    }

    private function canCancel(Reservation $subject): bool
    {
        $now = new \DateTime();

        if ($subject->getDateEnd() < $now ||
            $subject->getDateStart()->diff($now)->h <= 1 ||
            $subject->getDateStart() < $now
        ) {
            return false;
        }

        return true;
    }
}
