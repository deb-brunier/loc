<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(ReservationRepository $reservationRepository): JsonResponse
    {
        // TODO: protéger l'api avec une connexion pour empecher n'importer qui de récupérer les informations
        return new JsonResponse($reservationRepository->getScheduledReservation());
    }
}
