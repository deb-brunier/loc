<?php

namespace App\Form\Profile;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adresse email *',
                'empty_data' => '',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom *',
                'empty_data' => '',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom *',
                'empty_data' => '',
            ])
            ->add('companyName', TextType::class, [
                'label' => 'Nom de la société',
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'label' => 'Siret',
                'required' => false,
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse *',
                'empty_data' => '',
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Code postal *',
                'empty_data' => '',
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville *',
                'empty_data' => '',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone *',
                'empty_data' => '',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
