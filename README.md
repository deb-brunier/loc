<h2 align="center">1'LOC</h2>
<p align="center"><img src="https://img.shields.io/badge/html5-e34c26?&style=for-the-badge" alt="Html5" title="HTML 5" height="25"> <img src="https://img.shields.io/badge/css3-2965f1?&style=for-the-badge" alt="css3" title="CSS 3" height="25"> <img src="https://img.shields.io/badge/php-8993be?&style=for-the-badge" alt="php" title="php" height="25"> <img src="https://img.shields.io/badge/mysql-4479A1?&style=for-the-badge" alt="mysql" title="mysql" height="25"> <img src="https://img.shields.io/badge/Symfony-000000?&style=for-the-badge" alt="Symfony" title="Symfony" height="25"> <img src="https://img.shields.io/badge/bootstrap-563d7c?&style=for-the-badge" alt="bootstrap" title="bootstrap" height="25"> </p>

<p>
Ce projet est une application fictive de location en ligne de salles de travail.
</p>


<h3>DOCUMENTATION</h3>

- [1. Installation](docs/1_installation.md)
- [2. Environnements](docs/2_environnements.md)
- [3. Tests](docs/3_tests.md)
- [4. UML](docs/4_uml.md)


<h3>CONTRIBUTORS</h3>
If you would like to contribute to this repository, feel free to send a pull request, and I will review your code. Also feel free to post about any problems that may arise in the issues section of the repository.