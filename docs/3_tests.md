# Tests

* **Test unitaire**: tester unitairement un ensemble de classes
* **Test fonctionnel**: tester l'interface en simulant une requête HTTP

## Pour lancer les tests

```
php bin/phpunit --testdox
```
<br>

[Retour au sommaire](../README.md)