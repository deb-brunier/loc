# UML

  - [Cas d'utilisation](#cas-dutilisation)
  - [Diagramme de classe](#diagramme-de-classe)
  - [Sitemap](#sitemap)
___
## Cas d'utilisation
#### Visiteur
[![Cas d'utilisation](uml/img/usecase_visiteur.png)](uml/diagramme-cas-utilisation_visiteur.puml)
<br>
#### Client
[![Cas d'utilisation](uml/img/usecase_client.png)](uml/diagramme-cas-utilisation_client.puml)
<br>
#### Administrateur
[![Cas d'utilisation](uml/img/usecase_admin.png)](uml/diagramme-cas-utilisation_admin.puml)
<br>
#### Gestion des salles
[![Gestion des salles](uml/img/gestion-salle.png)](uml/gestion-salle.puml)
___
## Diagramme de classe
[![Diagramme de classe](uml/img/classe.png)](uml/diagramme-classe.puml)

___
## Sitemap

#### Visiteur
[![Sitmap visiteur](uml/img/visiteur.png)](uml/sitemap/sitemap_visiteur.puml)
<br>
#### Client
[![Sitmap client](uml/img/client.png)](uml/sitemap/sitemap_client.puml)
<br>
#### Administrateur
[![Sitmap employe](uml/img/administrateur.png)](uml/sitemap/sitemap_admin.puml)

<br>

[Retour au sommaire](../README.md)