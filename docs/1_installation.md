# Installation

## Récupérer les source du projet
```
git clone git@gitlab.com:deb-brunier/loc.git
```

## Pré-requis
* PHP >= 7.4
* Composer
* MySQL >= 5.7
* NodeJS >= 14.15
* npm >= 6.14
* yarn >= 1.22

## Installer les dépendances
Se positionner dans le dossier du projet :
```
cd 1loc
```

Executer les commandes suivantes :
```
composer install
yarn install
```

## Initialiser la base de données

Pour l'environnment de `prod` et `dev`
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Pour l'environnement de `test`
```
php bin/console doctrine:database:create --e test
php bin/console doctrine:schema:update --force
```

Import des données de développement
```
php bin/console doctrine:fixtures:load
```

## Lancer la compilation des assets
Compiler une seule fois les fichiers en environnement de développement :
```
yarn encore dev
```

Compilation automatique :
```
yarn watch
```

Compilation pour la production :
```
yarn encore production
```

## Lancer le serveur en local
Il est nécessaire d'avoir installé le [binaire de symfony](https://symfony.com/download).
```
symfony serve
````
<br>

[Retour au sommaire](../README.md)