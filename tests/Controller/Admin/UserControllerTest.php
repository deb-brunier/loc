<?php

namespace App\Tests\Controller\Admin;

use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends WebTestCase
{
    private const USER_URL = '/admin/utilisateur/';
    private const ACCESS_LEVEL = 'ROLE_ADMIN';

    public function testManageUserPage(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['user1'];
        $roles = $user->getRoles();
        array_push($roles, self::ACCESS_LEVEL);
        $user->setRoles($roles);
        $this->client->loginUser($user);

        $this->client->request('GET', self::USER_URL);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Liste des utilisateurs');

        //Checks that the table does not contain a link allowing to modify the connected user
        $this->assertStringNotContainsString(self::USER_URL.$user->getId().'/modifier', $this->client->getCrawler()->filter('table')->html());
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', self::USER_URL);
        $this->assertResponseRedirects('/connexion');
    }

    public function testManageUserMyAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $this->client->request('GET', self::USER_URL.$admin->getId().'/modifier');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testManageUserWithGoodValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::USER_URL.$users['paul']->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'user' => [
                'firstName' => 'new First Name',
                'lastName' => 'new Last Name',
                'email' => 'new@email.local',
                'phone' => '0612345678',
                'address' => 'new address',
                'zipCode' => '11111',
                'city' => 'new city',
            ],
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testManageUserWithExistingValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::USER_URL.$users['user1']->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'user' => [
                'email' => $users['user2']->getEmail(),
                'firstName' => 'new First Name',
                'lastName' => 'new Last Name',
                'phone' => '0612345678',
                'address' => 'new address',
                'zipCode' => '11111',
                'city' => 'new city',
            ],
        ]);

        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testManageUserWithEmptyValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::USER_URL.$users['user1']->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'user' => [
                'firstName' => '',
                'lastName' => '',
                'email' => '',
                'phone' => '',
                'address' => '',
                'zipCode' => '',
                'city' => '',
            ],
        ]);

        $this->client->submit($form);
        $this->expectFormErrors(7);
    }

    public function testDeleteUser(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::USER_URL.$users['user1']->getId().'/modifier');
        $form = $crawler->selectButton('Supprimer')->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testShowUser(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $user = $users['paul'];
        $this->client->request('GET', self::USER_URL.$user->getId());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', $user->getFullName());
    }
}
