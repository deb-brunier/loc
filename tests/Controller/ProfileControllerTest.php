<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Tests\WebTestCase;

class ProfileControllerTest extends WebTestCase
{
    private const PROFILE_URL = 'mon-compte';
    private const DELETE_ACCOUNT_BTN = 'Supprimer mon compte';
    private const PROFILE_FORM_SUBMIT = 'Modifier';

    public function testProfilePage(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $this->client->request('GET', self::PROFILE_URL);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Mon compte');
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', self::PROFILE_URL);
        $this->assertResponseRedirects('/connexion');
    }

    public function testUpdateProfileWithGoodValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'profile' => [
                'firstName' => 'new First Name',
                'lastName' => 'new Last Name',
                'email' => 'new@email.local',
                'phone' => '0612345678',
                'address' => 'new address',
                'zipCode' => '11111',
                'city' => 'new city',
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testUpdateProfileWithEmptyValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'profile' => [
                'firstName' => '',
                'lastName' => '',
                'email' => '',
                'phone' => '',
                'address' => '',
                'zipCode' => '',
                'city' => '',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(7);
    }

    public function testUpdateProfileWithWrongEmail(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'profile' => [
                'firstName' => 'new First Name',
                'lastName' => 'new Last Name',
                'email' => 'bad',
                'phone' => '0612345678',
                'address' => 'new address',
                'zipCode' => '11111',
                'city' => 'new city',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testUpdateProfileWithExistingValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'profile' => [
                'firstName' => 'new First Name',
                'lastName' => 'new Last Name',
                'email' => $users['user1']->getEmail(),
                'phone' => '0612345678',
                'address' => 'new address',
                'zipCode' => '11111',
                'city' => 'new city',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testUpdatePasswordWithGoodValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier-mot-de-passe');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'edit_password' => [
                'currentPassword' => 'hH2m3T7An',
                'password' => [
                    'first' => '1newPassword',
                    'second' => '1newPassword',
                ],
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testUpdatePasswordWithEmptyValue(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier-mot-de-passe');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'edit_password' => [
                'currentPassword' => '',
                'password' => [
                    'first' => '',
                    'second' => '',
                ],
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(2);
    }

    public function testUpdatePasswordWithBadCurrentPassword(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier-mot-de-passe');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'edit_password' => [
                'currentPassword' => 'sfsdfss',
                'password' => [
                    'first' => '1newPassword',
                    'second' => '1newPassword',
                ],
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testUpdatePasswordWithMismatchNewPassword(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier-mot-de-passe');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'edit_password' => [
                'currentPassword' => 'hH2m3T7An',
                'password' => [
                    'first' => '1newPasswordZ',
                    'second' => '1newPassword',
                ],
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testUpdatePasswordWithBadNewPassword(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::PROFILE_URL.'/modifier-mot-de-passe');
        $form = $crawler->selectButton(self::PROFILE_FORM_SUBMIT)->form([
            'edit_password' => [
                'currentPassword' => 'hH2m3T7An',
                'password' => [
                    'first' => 'ghdjkdsjh',
                    'second' => 'ghdjkdsjh',
                ],
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testShowReservation(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);
        /** @var User $user */
        $user = $data['user1'];
        $this->client->loginUser($user);
        $this->client->request('GET', self::PROFILE_URL.'/mes-reservations');
        $this->assertResponseIsSuccessful();
    }

    public function testCancelReservation(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);
        /** @var User $user */
        $user = $data['user1'];
        $this->client->loginUser($user);
        $crawler = $this->client->request('GET', self::PROFILE_URL.'/mes-reservations');
        $form = $crawler->selectButton('cancel')->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testDeleteAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $crawler = $this->client->request('GET', self::PROFILE_URL);
        $form = $crawler->selectButton(self::DELETE_ACCOUNT_BTN)->form();
        $this->client->submit($form);
        $this->assertResponseRedirects('/compte-supprime');
        $this->client->followRedirect();
        $this->assertSelectorTextContains('h1', 'Compte supprimé');
    }

    public function testDeleteAccountWithModifingId(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $crawler = $this->client->request('GET', self::PROFILE_URL);
        $form = $crawler->selectButton(self::DELETE_ACCOUNT_BTN)->form();
        $form->getNode()->setAttribute('action', self::PROFILE_URL.'/supprimer/'.$users['user2']->getId());
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }
}
