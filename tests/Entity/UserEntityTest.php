<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Tests\KernelTestCase;

class UserEntityTest extends KernelTestCase
{
    private function getEntity(): User
    {
        return (new User())
            ->setEmail('good@email.local')
            ->setPassword('1GoodPassword')
            ->setLastName('Good last name')
            ->setFirstName('Good first name')
            ->setPhone('0600000000')
            ->setAddress('38 rue good')
            ->setZipCode('38100')
            ->setCity('Good city')
        ;
    }

    public function testWithGoodValues(): void
    {
        $this->assertHasErrors($this->getEntity());
    }

    public function testWithEmptyValues(): void
    {
        $badUser = $this->getEntity()
            ->setEmail('')
            ->setPassword('')
            ->setLastName('')
            ->setFirstName('')
            ->setPhone('')
            ->setAddress('')
            ->setZipCode('')
            ->setCity('')
        ;
        $this->assertHasErrors($badUser, 8);
    }

    public function testWithNoNumberPassword(): void
    {
        $this->assertHasErrors($this->getEntity()->setPassword('badPassword'), 1);
    }

    public function testWithTooShortPassword(): void
    {
        $this->assertHasErrors($this->getEntity()->setPassword('short'), 1);
    }

    public function testWithNoUpperPassword(): void
    {
        $this->assertHasErrors($this->getEntity()->setPassword('1badpassword'), 1);
    }

    public function testWithNoLowerPassword(): void
    {
        $this->assertHasErrors($this->getEntity()->setPassword('1BADPASSWORD'), 1);
    }

    public function testWithBadEmail(): void
    {
        $this->assertHasErrors($this->getEntity()->setEmail('notAnEmail'), 1);
    }

    public function testAlreadyExist(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/user.yaml',
        ]);

        $this->assertHasErrors($this->getEntity()->setEmail($users['user1']->getEmail()), 1);
    }
}
